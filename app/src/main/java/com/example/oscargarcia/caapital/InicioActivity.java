package com.example.oscargarcia.caapital;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class InicioActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        Button button_sesion= (Button) findViewById(R.id.inicio_btn_sesion);
        Button button_registro= (Button) findViewById(R.id.inicio_btn_registrate);

        button_sesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(InicioActivity.this, LoginActivity.class));
                finish();
            }
        });

        button_registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(InicioActivity.this, RegisterActivity.class));
                finish();
            }
        });
    }
}
