package com.example.oscargarcia.caapital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

public class ConfigActivity extends AppCompatActivity {

    public static final int CONNECTION_TIMEOUT=10000;
    public static final int READ_TIMEOUT=15000;
    String load;
    public final static boolean isValidEmail(CharSequence target)
    {
        if (TextUtils.isEmpty(target))
        {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        final MyApplication mApp= ((MyApplication) getApplicationContext());
        final String email= mApp.getEmail();
        new AsyncLoad().execute(email);

        TextView txt_config= (TextView) findViewById(R.id.config_txt_config);
        TextView txt_name= (TextView) findViewById(R.id.config_txt_name);
        TextView txt_email= (TextView) findViewById(R.id.config_txt_email);
        TextView txt_pswd1= (TextView) findViewById(R.id.config_txt_pswd1);
        TextView txt_pswd2= (TextView) findViewById(R.id.config_txt_pswd2);
        TextView txt_bar= (TextView) findViewById(R.id.config_txt_bar) ;

        Button btn_config = (Button) findViewById(R.id.config_btn_config);
        Button btn_back= (Button) findViewById(R.id.config_btn_back);

        AssetManager am = getApplicationContext().getAssets();
        Typeface nunito = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "Nunito-Bold.ttf"));
        Typeface nunito_bold = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "Nunito-ExtraBold.ttf"));

        txt_config.setTypeface(nunito_bold);
        txt_name.setTypeface(nunito);
        txt_email.setTypeface(nunito);
        txt_pswd1.setTypeface(nunito);
        txt_pswd2.setTypeface(nunito);
        txt_bar.setTypeface(nunito_bold);

        final EditText edit_name= (EditText) findViewById(R.id.config_edit_name);
        final EditText edit_email= (EditText) findViewById(R.id.config_edit_email);
        final EditText edit_pswd1= (EditText) findViewById(R.id.config_edit_pswd1);
        final EditText edit_pswd2= (EditText) findViewById(R.id.config_edit_pswd2);



        edit_email.setEnabled(false);
        edit_email.setText(email);

        btn_back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(ConfigActivity.this, CappPrincipal.class);
                startActivity(intent);
                finish();
            }
        });

        btn_config.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (edit_pswd1.getText().length()==0 && edit_pswd2.getText().length()==0)
                {
                    if (edit_name.getText().toString().matches("[a-zA-Z0-9.? ]*") && edit_name.getText().toString().length()>5)
                    {
                        new AsyncName().execute(email,edit_name.getText().toString());
                    }
                    else
                    {
                        Toast.makeText(ConfigActivity.this,"Nombre de al menos 6 caracteras y sin simbolos",Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    String pswd= edit_pswd1.getText().toString();
                    if (pswd.matches("[a-zA-Z0-9.? ]*") && pswd.length()>5)
                    {
                        if (edit_pswd1.getText().toString().equals(edit_pswd2.getText().toString()))
                        {

                            if (edit_name.getText().toString().matches("[a-zA-Z0-9.? ]*") && edit_name.getText().toString().length()>5)
                            {
                                new AsyncConfig().execute(email,edit_name.getText().toString(),pswd);
                            }
                            else
                            {
                                Toast.makeText(ConfigActivity.this,"Nombre de al menos 6 caracteras y sin simbolos",Toast.LENGTH_SHORT).show();
                            }



                        }
                        else
                        {
                            Toast.makeText(ConfigActivity.this,"Las contraseñas no son iguales",Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(ConfigActivity.this,"Contraseñas de al menos 6 caracteras y sin simbolos",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


    }
    private class AsyncLoad extends AsyncTask<String, String, String>
    {
        ProgressDialog pdLoading = new ProgressDialog(ConfigActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            //this method will be running on UI thread
             pdLoading.setMessage("\tCargando...");
             pdLoading.setCancelable(false);
             pdLoading.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            try {

                // Enter URL address where your php file resides
                url = new URL("https://cappital.000webhostapp.com/config.php");

            } catch (MalformedURLException e)
            {

                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("EMAIL", params[0])
                        .appendQueryParameter("QUERY","LOAD");
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {

                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            //this method will be running on UI thread
            pdLoading.dismiss();
            Log.d("RESULT:",result);
            try
            {
                JSONObject json_output = new JSONObject(result);
                //Log.d("A:","logged");
                ((EditText) findViewById(R.id.config_edit_name)).setText(json_output.getString("NAME"));
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            //Log.d("RESULT:",result);
        }

    }
    private class AsyncConfig extends AsyncTask<String, String, String>
    {
        //ProgressDialog pdLoading = new ProgressDialog(LoginActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            //this method will be running on UI thread
            // pdLoading.setMessage("\tCargando...");
            // pdLoading.setCancelable(false);
            // pdLoading.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            try {

                // Enter URL address where your php file resides
                url = new URL("https://cappital.000webhostapp.com/config.php");

            } catch (MalformedURLException e)
            {

                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("EMAIL", params[0])
                        .appendQueryParameter("NAME",params[1])
                        .appendQueryParameter("PASSWORD",params[2])
                        .appendQueryParameter("QUERY","CONFIG");
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {

                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            //this method will be running on UI thread
            //pdLoading.dismiss();
            Log.d("RESULT:",result);
            Toast.makeText(ConfigActivity.this,"Cambios guardados",Toast.LENGTH_SHORT).show();
        }

    }
    private class AsyncName extends AsyncTask<String, String, String>
    {
        //ProgressDialog pdLoading = new ProgressDialog(LoginActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            //this method will be running on UI thread
            // pdLoading.setMessage("\tCargando...");
            // pdLoading.setCancelable(false);
            // pdLoading.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            try {

                // Enter URL address where your php file resides
                url = new URL("https://cappital.000webhostapp.com/config.php");

            } catch (MalformedURLException e)
            {

                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("EMAIL", params[0])
                        .appendQueryParameter("NAME",params[1])
                        .appendQueryParameter("QUERY","CONFIG_NAME");
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {

                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            //this method will be running on UI thread
            //pdLoading.dismiss();
            Log.d("RESULT:",result);
            Toast.makeText(ConfigActivity.this,"Cambios guardados",Toast.LENGTH_SHORT).show();
            //Log.d("RESULT:",result);
        }

    }
}
