package com.example.oscargarcia.caapital;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

public class MetaAhorro extends AppCompatActivity implements View.OnClickListener{

    public static final int CONNECTION_TIMEOUT=10000;
    public static final int READ_TIMEOUT=15000;
    public static int iconId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meta_ahorro);

        AssetManager am = getApplicationContext().getAssets();
        Typeface nunito = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "Nunito-Bold.ttf"));

        iconId = -1;

        final MyApplication mApp= ((MyApplication) getApplicationContext());
        final String email= mApp.getEmail();

        Button btn_atras= (Button) findViewById(R.id.Btn_atras);
        Button btn_aceptar_meta= (Button) findViewById(R.id.Btn_aceptarMeta);
        Button btn_perfil = (Button) findViewById(R.id.Btn_perfil);
        final EditText etx_nom_meta= (EditText) findViewById(R.id.Etx_nomMeta);
        final EditText etx_cant_meta= (EditText) findViewById(R.id.Etx_cantMeta);
        final EditText etx_fecha_meta= (EditText) findViewById(R.id.Etx_fechaMeta);
        final int tipo=3;
        Button btn_icono1= (Button) findViewById(R.id.Btn_icono1);
        btn_icono1.setOnClickListener(this);
        Button btn_icono2= (Button) findViewById(R.id.Btn_icono2);
        btn_icono2.setOnClickListener(this);
        Button btn_icono3= (Button) findViewById(R.id.Btn_icono3);
        btn_icono3.setOnClickListener(this);
        Button btn_icono4= (Button) findViewById(R.id.Btn_icono4);
        btn_icono4.setOnClickListener(this);
        Button btn_icono5= (Button) findViewById(R.id.Btn_icono5);
        btn_icono5.setOnClickListener(this);
        Button btn_icono6= (Button) findViewById(R.id.Btn_icono6);
        btn_icono6.setOnClickListener(this);
        Button btn_icono7= (Button) findViewById(R.id.Btn_icono7);
        btn_icono7.setOnClickListener(this);
        Button btn_icono8= (Button) findViewById(R.id.Btn_icono8);
        btn_icono8.setOnClickListener(this);
        Button btn_icono9= (Button) findViewById(R.id.Btn_icono9);
        btn_icono9.setOnClickListener(this);
        Button btn_icono10= (Button) findViewById(R.id.Btn_icono10);
        btn_icono10.setOnClickListener(this);
        Button btn_icono11= (Button) findViewById(R.id.Btn_icono11);
        btn_icono11.setOnClickListener(this);
        Button btn_icono12= (Button) findViewById(R.id.Btn_icono12);
        btn_icono12.setOnClickListener(this);
        Button btn_icono13= (Button) findViewById(R.id.Btn_icono13);
        btn_icono13.setOnClickListener(this);
        Button btn_icono14= (Button) findViewById(R.id.Btn_icono14);
        btn_icono14.setOnClickListener(this);
        Button btn_icono15= (Button) findViewById(R.id.Btn_icono15);
        btn_icono15.setOnClickListener(this);

        btn_perfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(MetaAhorro.this, ConfigActivity.class);
                startActivity(intent);
                finish();

            }
        });

        btn_atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(MetaAhorro.this, CappPrincipal.class);
                startActivity(intent);
                finish();
            }
        });

        btn_aceptar_meta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if (etx_nom_meta.getText().toString().matches("") || etx_cant_meta.getText().toString().matches("") || etx_fecha_meta.getText().toString().matches(""))
                {
                    Toast.makeText(MetaAhorro.this,"Llena los campos",Toast.LENGTH_SHORT).show();

                }

                else {
                    if (etx_fecha_meta.getText().toString().matches("([0-9]{4})/([0-9]{2})/([0-9]{2})")) {
                        if (iconId == -1) {
                            Toast.makeText(MetaAhorro.this, "Selecciona un Icono", Toast.LENGTH_SHORT).show();
                        } else {

                            String meta="-$"+etx_cant_meta.getText().toString();
                            new MetaAhorro.AsyncEntrada().execute(etx_nom_meta.getText().toString(),
                                    meta, etx_fecha_meta.getText().toString(), Integer.toString(tipo), email);
                            //mApp.setEmail(text_email.getText().toString());
                        }
                    } else {
                        Toast.makeText(MetaAhorro.this, "Ingrese: yyyy/mm/dd", Toast.LENGTH_SHORT).show();



                    }
                }
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case  R.id.Btn_icono1: {
                iconId = 1;
                break;
            }

            case R.id.Btn_icono2: {
                iconId = 2;
                break;
            }

            case R.id.Btn_icono3: {
                iconId = 3;
                break;
            }

            case R.id.Btn_icono4: {
                iconId = 4;
                break;
            }

            case R.id.Btn_icono5: {
                iconId = 5;
                break;
            }

            case R.id.Btn_icono6: {
                iconId = 6;
                break;
            }

            case R.id.Btn_icono7: {
                iconId = 7;
                break;
            }

            case R.id.Btn_icono8: {
                iconId = 8;
                break;
            }

            case R.id.Btn_icono9: {
                iconId = 9;
                break;
            }

            case R.id.Btn_icono10: {
                iconId = 10;
                break;
            }

            case R.id.Btn_icono11: {
                iconId = 11;
                break;
            }

            case R.id.Btn_icono12: {
                iconId = 12;
                break;
            }

            case R.id.Btn_icono13: {
                iconId = 13;
                break;
            }

            case R.id.Btn_icono14: {
                iconId = 14;
                break;
            }

            case R.id.Btn_icono15: {
                iconId = 15;
                break;
            }
        }
    }


    private class AsyncEntrada extends AsyncTask<String, String, String>
    {
        //ProgressDialog pdLoading = new ProgressDialog(LoginActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            //this method will be running on UI thread
            //pdLoading.setMessage("\tLoading...");
            //pdLoading.setCancelable(false);
            //pdLoading.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            try {

                // Enter URL address where your php file resides
                url = new URL("https://cappital.000webhostapp.com/entrada.php");

            } catch (MalformedURLException e)
            {

                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("NOMBRE", params[0])
                        .appendQueryParameter("CANTIDAD", params[1])
                        .appendQueryParameter("FECHA",params[2])
                        .appendQueryParameter("TIPO",params[3])
                        .appendQueryParameter("ICONID", Integer.toString(iconId))
                        .appendQueryParameter("QUERY","ENTRADA")
                        .appendQueryParameter("EMAIL", params[4]);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();


            } catch (IOException e1) {

                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            System.out.println(result);
            Intent intent = new Intent(MetaAhorro.this, CappPrincipal.class);
            startActivity(intent);
            finish();
        }
    }
}
