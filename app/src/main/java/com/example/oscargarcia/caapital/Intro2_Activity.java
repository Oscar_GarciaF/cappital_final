package com.example.oscargarcia.caapital;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.koushikdutta.ion.Ion;

public class Intro2_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro2_);

        getWindow().setStatusBarColor(getResources().getColor(R.color.Verde));

        ImageView imageView = (ImageView) findViewById(R.id.Image_intro2);
        Ion.with(imageView).load("file:///android_asset/Intro2.gif");


        Button button_next= (Button) findViewById(R.id.btn_intro2_next);
        Button button_skip= (Button) findViewById(R.id.btn_intro2_skip);
        Button button_back= (Button) findViewById(R.id.btn_intro2_back);

        RelativeLayout layout= (RelativeLayout)findViewById(R.id.activity_intro2_);
        final boolean register= getIntent().getExtras().getBoolean("register");

        layout.setOnTouchListener(new OnSwipeTouchListener(Intro2_Activity.this) {

            public void onSwipeRight()
            {
                startActivity(new Intent(Intro2_Activity.this, Intro1_Activity.class).putExtra("register",register));

                Intro2_Activity.this.overridePendingTransition(R.anim.anim_renter,R.anim.anim_rleave);
                finish();
            }

            public void onSwipeLeft()
            {
                startActivity(new Intent(Intro2_Activity.this, Intro3_Activity.class).putExtra("register",register));
                Intro2_Activity.this.overridePendingTransition(R.anim.anim_enter,R.anim.anim_leave);
                finish();
            }
        });

        button_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(Intro2_Activity.this, Intro3_Activity.class).putExtra("register",register));
                Intro2_Activity.this.overridePendingTransition(R.anim.anim_enter,R.anim.anim_leave);
                finish();
            }
        });

        button_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (register)
                {
                    startActivity(new Intent(Intro2_Activity.this, RegisterActivity.class));
                    Intro2_Activity.this.overridePendingTransition(R.anim.anim_enter,R.anim.anim_leave);
                    finish();
                }
                else
                {
                    startActivity(new Intent(Intro2_Activity.this, CappPrincipal.class));
                    Intro2_Activity.this.overridePendingTransition(R.anim.anim_enter,R.anim.anim_leave);
                    finish();
                }

            }
        });

        button_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(Intro2_Activity.this, Intro1_Activity.class).putExtra("register",register));
                Intro2_Activity.this.overridePendingTransition(R.anim.anim_renter,R.anim.anim_rleave);
                finish();
            }
        });



    }
}
