package com.example.oscargarcia.caapital;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

public class LoginActivity extends AppCompatActivity
{
    public static final int CONNECTION_TIMEOUT=10000;
    public static final int READ_TIMEOUT=15000;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        AssetManager am = getApplicationContext().getAssets();
        Typeface nunito = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "Nunito-Bold.ttf"));

        Button button_iniciar_sesion= (Button) findViewById(R.id.login_btn_iniciarsesion);
        Button button_registrarme= (Button) findViewById(R.id.login_btn_registrarme);
        final EditText text_email= (EditText) findViewById(R.id.login_edit_user);
        final EditText text_password= (EditText) findViewById(R.id.login_edit_password);
        TextView txt_correo= (TextView) findViewById(R.id.login_txt_correo);
        TextView txt_pswd= (TextView) findViewById(R.id.login_txt_contraseña);

        txt_correo.setTypeface(nunito);
        txt_pswd.setTypeface(nunito);

        final MyApplication mApp= ((MyApplication) getApplicationContext());

        button_iniciar_sesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (isValidEmail(text_email.getText()))
                {
                    mApp.setEmail(text_email.getText().toString());
                    new AsyncLogin().execute(text_email.getText().toString(), text_password.getText().toString());
                }
                else
                {
                    Toast.makeText(LoginActivity.this, "Porfavor escriba un Email valido", Toast.LENGTH_SHORT).show();
                }

            }
        });

        button_registrarme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
    public final static boolean isValidEmail(CharSequence target)
    {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private class AsyncLogin extends AsyncTask<String, String, String>
    {
        //ProgressDialog pdLoading = new ProgressDialog(LoginActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            //this method will be running on UI thread
           // pdLoading.setMessage("\tCargando...");
           // pdLoading.setCancelable(false);
           // pdLoading.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            try {

                // Enter URL address where your php file resides
                url = new URL("https://cappital.000webhostapp.com/users.php");

            } catch (MalformedURLException e)
            {

                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("EMAIL", params[0])
                        .appendQueryParameter("PASSWORD", params[1])
                        .appendQueryParameter("QUERY","LOGIN");
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {

                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            //this method will be running on UI thread
            //pdLoading.dismiss();
            Log.d("RESULT:",result);
            try
            {
                JSONObject json_output = new JSONObject(result);
                //Log.d("A:","logged");

                if (!json_output.getBoolean("error"))
                {
                    Intent intent = new Intent(LoginActivity.this, Intro1_Activity.class);
                    intent.putExtra("register",false);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Toast.makeText(LoginActivity.this, "Email/Contraseña incorrectos", Toast.LENGTH_SHORT).show();
                }

            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            //Log.d("RESULT:",result);
        }

    }

}
