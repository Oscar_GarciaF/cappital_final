package com.example.oscargarcia.caapital;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

public class SalidaDinero extends AppCompatActivity {

    public static final int CONNECTION_TIMEOUT=10000;
    public static final int READ_TIMEOUT=15000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salida_dinero);

        AssetManager am = getApplicationContext().getAssets();
        Typeface nunito = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "Nunito-Bold.ttf"));

        final MyApplication mApp= ((MyApplication) getApplicationContext());
        final String email= mApp.getEmail();

        Button btn_atras= (Button) findViewById(R.id.Btn_atras);
        Button btn_aceptar_entrada= (Button) findViewById(R.id.Btn_aceptarSalida);
        Button btn_perfil = (Button) findViewById(R.id.Btn_perfil);
        final EditText etx_nom_salida= (EditText) findViewById(R.id.Etx_nomSalida);
        final EditText etx_cant_salida= (EditText) findViewById(R.id.Etx_cantSalida);
        final EditText etx_fecha_salida= (EditText) findViewById(R.id.Etx_fechaSalida);
        final int tipo=2;

        btn_perfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(SalidaDinero.this, ConfigActivity.class);
                startActivity(intent);
                finish();

            }
        });

        btn_atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(SalidaDinero.this, CappPrincipal.class);
                startActivity(intent);
                finish();
            }
        });

        btn_aceptar_entrada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if (etx_nom_salida.getText().toString().matches("") || etx_cant_salida.getText().toString().matches("") || etx_fecha_salida.getText().toString().matches(""))
                {
                    Toast.makeText(SalidaDinero.this,"Llena los campos",Toast.LENGTH_SHORT).show();

                }
                else
                {
                    if (etx_fecha_salida.getText().toString().matches("([0-9]{4})/([0-9]{2})/([0-9]{2})"))
                    {
                    String salida="-$"+etx_cant_salida.getText().toString();
                    new SalidaDinero.AsyncSalida().execute(etx_nom_salida.getText().toString(),
                            salida,etx_fecha_salida.getText().toString(), Integer.toString(tipo), email);
                    //mApp.setEmail(text_email.getText().toString());

                    } else {
                        Toast.makeText(SalidaDinero.this, "Ingrese: yyyy/mm/dd", Toast.LENGTH_SHORT).show();

                    }

                }



            }
        });



    }

    private class AsyncSalida extends AsyncTask<String, String, String>
    {
        //ProgressDialog pdLoading = new ProgressDialog(LoginActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            //this method will be running on UI thread
            //pdLoading.setMessage("\tLoading...");
            //pdLoading.setCancelable(false);
            //pdLoading.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            try {

                // Enter URL address where your php file resides
                url = new URL("https://cappital.000webhostapp.com/entrada.php");

            } catch (MalformedURLException e)
            {

                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("NOMBRE", params[0])
                        .appendQueryParameter("CANTIDAD", params[1])
                        .appendQueryParameter("FECHA",params[2])
                        .appendQueryParameter("TIPO",params[3])
                        .appendQueryParameter("ICONID","0")
                        .appendQueryParameter("QUERY","ENTRADA")
                        .appendQueryParameter("EMAIL", params[4]);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();


            } catch (IOException e1) {

                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result)
        {

            Intent intent = new Intent(SalidaDinero.this, CappPrincipal.class);
            startActivity(intent);
            finish();
        }
    }
}
