package com.example.oscargarcia.caapital;

/**
 * Created by Luis Miguel on 13/05/2017.
 */

public class InformacionDinero {
    String nombre;
    String cantidad;
    String fecha;
    int tipo;

    public InformacionDinero(String nombre, String cantidad, String fecha, int tipo) {
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.fecha = fecha;
        this.tipo=tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
}
