package com.example.oscargarcia.caapital;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class LoadingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        AssetManager am = getApplicationContext().getAssets();
        Typeface nunito = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "Nunito-Bold.ttf"));
        Typeface nunito_bold = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "Nunito-ExtraBold.ttf"));

        TextView txt_header= (TextView) findViewById(R.id.txt_loading_header);
        TextView txt_tip1= (TextView) findViewById(R.id.txt_loading_tip1);
        TextView txt_tip2= (TextView) findViewById(R.id.txt_loading_tip2);
        ImageView img= (ImageView) findViewById(R.id.imageView_loading_gif);

        txt_header.setTypeface(nunito_bold);
        txt_tip1.setTypeface(nunito_bold);
        txt_tip2.setTypeface(nunito);
        Ion.with(img).load("file:///android_asset/Monedita_Cargandose.gif");

        TimerTask task = new TimerTask() {

            @Override
            public void run() {
                Intent intent = new Intent(LoadingActivity.this, InicioActivity.class);
                startActivity(intent);
                finish();
            }
        };
        Timer t = new Timer();
        t.schedule(task, 5900);




    }
}
