package com.example.oscargarcia.caapital;


import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.koushikdutta.ion.Ion;


public class Intro1_Activity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro1_);

        ImageView imageView = (ImageView) findViewById(R.id.Image_intro1);
        Ion.with(imageView).load("file:///android_asset/Intro1.gif");

        getWindow().setStatusBarColor(getResources().getColor(R.color.AzulClaro));


        Button button_next= (Button) findViewById(R.id.btn_intro1_next);
        Button button_skip= (Button) findViewById(R.id.btn_intro1_skip);

        RelativeLayout layout= (RelativeLayout)findViewById(R.id.activity_intro1_);

        final boolean register= getIntent().getExtras().getBoolean("register");

        layout.setOnTouchListener(new OnSwipeTouchListener(Intro1_Activity.this) {

            public void onSwipeLeft()
            {
                startActivity(new Intent(Intro1_Activity.this, Intro2_Activity.class).putExtra("register",register));
                Intro1_Activity.this.overridePendingTransition(R.anim.anim_enter,R.anim.anim_leave);
                finish();
            }
        });

        button_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(Intro1_Activity.this, Intro2_Activity.class).putExtra("register",register));
                Intro1_Activity.this.overridePendingTransition(R.anim.anim_enter,R.anim.anim_leave);
                finish();
            }
        });

        button_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (register)
                {
                    startActivity(new Intent(Intro1_Activity.this, RegisterActivity.class));
                    Intro1_Activity.this.overridePendingTransition(R.anim.anim_enter,R.anim.anim_leave);
                    finish();
                }
                else
                {
                    startActivity(new Intent(Intro1_Activity.this, CappPrincipal.class));
                    Intro1_Activity.this.overridePendingTransition(R.anim.anim_enter,R.anim.anim_leave);
                    finish();
                }

            }
        });





    }
}
