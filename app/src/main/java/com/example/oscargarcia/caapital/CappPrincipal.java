package com.example.oscargarcia.caapital;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CappPrincipal extends AppCompatActivity {
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;

    List<InformacionDinero> ListadoRegistros = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capp_principal);

        final MyApplication mApp= ((MyApplication) getApplicationContext());
        final String email= mApp.getEmail();

        Button btn_agregar_entrada = (Button) findViewById(R.id.Btn_agregarEntrada);
        Button btn_agregar_salida = (Button) findViewById(R.id.Btn_agregarSalida);
        Button btn_agregar_meta = (Button) findViewById(R.id.Btn_agregarMeta);
        Button btn_perfil = (Button) findViewById(R.id.Btn_perfil);
        Button btn_atras = (Button) findViewById(R.id.Btn_atras);

        btn_perfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(CappPrincipal.this, ConfigActivity.class);
                startActivity(intent);
                finish();

            }
        });

        btn_agregar_entrada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CappPrincipal.this, EntradaDinero.class);
                startActivity(intent);
                finish();
            }
        });

        btn_agregar_salida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CappPrincipal.this, SalidaDinero.class);
                startActivity(intent);
                finish();
            }
        });

        btn_agregar_meta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CappPrincipal.this, MetaAhorro.class);
                startActivity(intent);
                finish();
            }
        });
        btn_atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(CappPrincipal.this, InicioActivity.class);
                startActivity(intent);
                finish();

            }
        });

        new AsyncTabla().execute(email);


    }

    private class AsyncTabla extends AsyncTask<String, String, String> {
        //ProgressDialog pdLoading = new ProgressDialog(LoginActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread
            //pdLoading.setMessage("\tLoading...");
            //pdLoading.setCancelable(false);
            //pdLoading.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                // Enter URL address where your php file resides
                url = new URL("https://cappital.000webhostapp.com/obtener.php");

            } catch (MalformedURLException e) {

                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("EMAIL", params[0]);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();


            } catch (IOException e1) {

                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {

                    return ("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            //Toast.makeText(EntradaDinero.this,result,Toast.LENGTH_SHORT).show();
            System.out.println(result);

            try {
                //JSONObject json_output = new JSONObject(result);
                System.out.println(result);
                //if (!json_output.getBoolean("error")) {
                JSONArray aJson = new JSONArray(result);
                    for (int i = 0; i < aJson.length(); i++) {
                        JSONObject json = aJson.getJSONObject(i);
                        String nombre = json.getString("Nombre");
                        String cantidad = json.getString("Cantidad");
                        String fecha = json.getString("Fecha");
                        int tipo = json.getInt("Tipo");
                        ListadoRegistros.add(new InformacionDinero(nombre, cantidad, fecha, tipo));
                    }
                //}

                AdaptadorTabla adapter = new AdaptadorTabla(CappPrincipal.this, ListadoRegistros);
                ListView Listview1 = (ListView) findViewById(R.id.ListView1);
                Listview1.setAdapter(adapter);

            } catch (JSONException e) {
                e.printStackTrace();
                System.out.println("error Json");
            }
        }

    }

    class AdaptadorTabla extends ArrayAdapter<InformacionDinero> {

        public AdaptadorTabla(Context context, List<InformacionDinero> datos) {
            super(context, R.layout.activity_list_item, datos);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View item = convertView;
            InformacionHolder holder;

            if (item == null) {
                LayoutInflater inflater = LayoutInflater.from(getContext());
                item = inflater.inflate(R.layout.activity_list_item, null);

                // Se mapea cada item del listado con los controles del layout
                holder = new InformacionHolder();
                holder.Nombre = (TextView) item.findViewById(R.id.txt_Nombre);
                holder.Cantidad = (TextView) item.findViewById(R.id.txt_Cantidad);
                holder.Fecha = (TextView) item.findViewById(R.id.txt_Fecha);
                item.setTag(holder);
            } else {
                holder = (InformacionHolder) item.getTag();
            }

            // lstListadoRegistros es el listado con los resultados
            final InformacionDinero registro = ListadoRegistros.get(position);
            holder.Nombre.setText(registro.getNombre());
            holder.Cantidad.setText(String.valueOf(registro.getCantidad()));
            holder.Fecha.setText(String.valueOf(registro.getFecha()));
             if(registro.getTipo()==1)
             {
                 holder.Nombre.setTextColor(getContext().getResources().getColor(R.color.verde));
                 holder.Cantidad.setTextColor(getContext().getResources().getColor(R.color.verde));
                 holder.Fecha.setTextColor(getContext().getResources().getColor(R.color.verde));

             }
            else if(registro.getTipo()==2)
            {
                holder.Nombre.setTextColor(getContext().getResources().getColor(R.color.rojo));
                holder.Cantidad.setTextColor(getContext().getResources().getColor(R.color.rojo));
                holder.Fecha.setTextColor(getContext().getResources().getColor(R.color.rojo));
            }
            else if(registro.getTipo()==3)
            {
                holder.Nombre.setTextColor(getContext().getResources().getColor(R.color.azulClaro));
                holder.Cantidad.setTextColor(getContext().getResources().getColor(R.color.azulClaro));
                holder.Fecha.setTextColor(getContext().getResources().getColor(R.color.azulClaro));
            }


            return (item);
        }
    }

    // Esto nos ayudará a tener un listview más completo
    // se crean los controles que se inflarán por cada item del listview
    class InformacionHolder {
        TextView Nombre;
        TextView Cantidad;
        TextView Fecha;
    }

}