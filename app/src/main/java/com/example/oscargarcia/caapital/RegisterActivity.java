package com.example.oscargarcia.caapital;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

public class RegisterActivity extends AppCompatActivity
{
    public static final int CONNECTION_TIMEOUT=10000;
    public static final int READ_TIMEOUT=15000;


    public final static boolean isValidEmail(CharSequence target)
    {
        if (TextUtils.isEmpty(target))
        {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getWindow().setStatusBarColor(getResources().getColor(R.color.Amarillo));

        Button button_conoce= (Button) findViewById(R.id.register_btn_conoce);
        Button button_crear= (Button) findViewById(R.id.register_btn_crear);
        Button button_back= (Button) findViewById(R.id.register_btn_back);
        final EditText text_name= (EditText) findViewById(R.id.register_txt_name);
        final EditText text_email= (EditText) findViewById(R.id.register_txt_email);
        final EditText text_password1= (EditText) findViewById(R.id.register_txt_pswd1);
        final EditText text_password2= (EditText) findViewById(R.id.register_txt_pswd2);

        TextView txt1= (TextView) findViewById(R.id.register_txt1);
        TextView txt2= (TextView) findViewById(R.id.register_txt2);
        TextView txt3= (TextView) findViewById(R.id.register_txt3);
        TextView txt4= (TextView) findViewById(R.id.register_txt4);

        AssetManager am = getApplicationContext().getAssets();
        Typeface nunito = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "Nunito-Bold.ttf"));

        txt1.setTypeface(nunito);
        txt2.setTypeface(nunito);
        txt3.setTypeface(nunito);
        txt4.setTypeface(nunito);

        final MyApplication mApp= ((MyApplication) getApplicationContext());

        button_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(RegisterActivity.this, InicioActivity.class);
                startActivity(intent);
                finish();
            }
        });




        button_conoce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(RegisterActivity.this, Intro1_Activity.class);
                intent.putExtra("register",true);
                startActivity(intent);
                finish();
            }
        });

        button_crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if (isValidEmail(text_email.getText()))
                {

                    String pswd=text_password1.getText().toString();
                    if (pswd.matches("[a-zA-Z0-9.? ]*") && pswd.length()>5)
                    {
                        if (text_password1.getText().toString().equals(text_password2.getText().toString()))
                        {
                            String name= text_name.getText().toString();
                            if (name.matches("[a-zA-Z0-9.? ]*") && name.length()>5)
                            {
                                mApp.setEmail(text_email.getText().toString());
                                new AsyncRegister().execute(text_email.getText().toString(),
                                        pswd,name);
                            }
                            else
                            {
                                Toast.makeText(RegisterActivity.this,"Nombre de al menos 6 caracteras y sin simbolos",Toast.LENGTH_SHORT).show();
                            }



                        }
                        else
                        {
                            Toast.makeText(RegisterActivity.this,"Las contraseñas no son iguales",Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(RegisterActivity.this,"Contraseñas de al menos 6 caracteras y sin simbolos",Toast.LENGTH_SHORT).show();
                    }


                }
                else
                {
                    Toast.makeText(RegisterActivity.this,"Escriba un Email válido",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private class AsyncRegister extends AsyncTask<String, String, String>
    {
        //ProgressDialog pdLoading = new ProgressDialog(LoginActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            //this method will be running on UI thread
            //pdLoading.setMessage("\tLoading...");
            //pdLoading.setCancelable(false);
            //pdLoading.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            try {

                // Enter URL address where your php file resides
                url = new URL("https://cappital.000webhostapp.com/users.php");

            } catch (MalformedURLException e)
            {

                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("EMAIL", params[0])
                        .appendQueryParameter("PASSWORD", params[1])
                        .appendQueryParameter("NAME",params[2])
                        .appendQueryParameter("QUERY","REGISTER");
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();


            } catch (IOException e1) {

                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            Log.d("RESULT:",result);
            try
            {
                JSONObject json_output = new JSONObject(result);
                //Log.d("RESULT:",result);


                if (!json_output.getBoolean("error"))
                {
                    Intent intent = new Intent(RegisterActivity.this, CappPrincipal.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Toast.makeText(RegisterActivity.this, "El email ya esta ocupado", Toast.LENGTH_SHORT).show();
                }

            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }


        }

    }

}
