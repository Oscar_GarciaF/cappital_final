package com.example.oscargarcia.caapital;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.koushikdutta.ion.Ion;

public class Intro3_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro3_);
        ImageView imageView = (ImageView) findViewById(R.id.Image_intro3);
        Ion.with(imageView).load("file:///android_asset/Intro3.gif");

        getWindow().setStatusBarColor(getResources().getColor(R.color.Rojo));


        Button button_next= (Button) findViewById(R.id.btn_intro3_next);
        Button button_skip= (Button) findViewById(R.id.btn_intro3_skip);
        Button button_back= (Button) findViewById(R.id.btn_intro3_back);

        RelativeLayout layout= (RelativeLayout)findViewById(R.id.activity_intro3_);
        final boolean register= getIntent().getExtras().getBoolean("register");

        layout.setOnTouchListener(new OnSwipeTouchListener(Intro3_Activity.this) {

            public void onSwipeRight()
            {
                startActivity(new Intent(Intro3_Activity.this, Intro2_Activity.class).putExtra("register",register));
                Intro3_Activity.this.overridePendingTransition(R.anim.anim_renter,R.anim.anim_rleave);
                finish();
            }

            public void onSwipeLeft()
            {
                if (register)
                {
                    startActivity(new Intent(Intro3_Activity.this, RegisterActivity.class));
                    Intro3_Activity.this.overridePendingTransition(R.anim.anim_enter,R.anim.anim_leave);
                    finish();
                }
                else
                {
                    startActivity(new Intent(Intro3_Activity.this, CappPrincipal.class));
                    Intro3_Activity.this.overridePendingTransition(R.anim.anim_enter,R.anim.anim_leave);
                    finish();
                }

            }
        });

        button_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (register)
                {
                    startActivity(new Intent(Intro3_Activity.this, RegisterActivity.class));
                    Intro3_Activity.this.overridePendingTransition(R.anim.anim_enter,R.anim.anim_leave);
                    finish();
                }
                else
                {
                    startActivity(new Intent(Intro3_Activity.this, CappPrincipal.class));
                    Intro3_Activity.this.overridePendingTransition(R.anim.anim_enter,R.anim.anim_leave);
                    finish();
                }
            }
        });

        button_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (register)
                {
                    startActivity(new Intent(Intro3_Activity.this, RegisterActivity.class));
                    Intro3_Activity.this.overridePendingTransition(R.anim.anim_enter,R.anim.anim_leave);
                    finish();
                }
                else
                {
                    startActivity(new Intent(Intro3_Activity.this, CappPrincipal.class));
                    Intro3_Activity.this.overridePendingTransition(R.anim.anim_enter,R.anim.anim_leave);
                    finish();
                }
            }
        });

        button_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(Intro3_Activity.this, Intro2_Activity.class).putExtra("register",register));
                Intro3_Activity.this.overridePendingTransition(R.anim.anim_renter,R.anim.anim_rleave);
                finish();
            }
        });



    }
}
